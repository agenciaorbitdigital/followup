<?php

	require 'modulos/session-login.php';

	require 'modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/contas-bancarias.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("TOPBAR", "templates/topbar.html");

	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SIDEBAR", "templates/sidebar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("FOOTER", "templates/footer.html");
	
	
	//Dados Usuário
	$email_login = $_SESSION['email_login'];
	$token = $_SESSION['TokenCorretora'];
					
	$tpl->THEME = $_SESSION['THMsystem'];
	$tpl->BACKGROUND = $_SESSION['BGsystem'];
	$tpl->COLOR = $_SESSION['CLRsystem'];

	$query_usuario = $pdo->query("SELECT id, nome, data_criacao, foto_perfil FROM cadastro_usuarios WHERE email = '$email_login'")->fetchAll();
 		
		foreach($query_usuario as $linha){
			$id_usuario = $linha['id'];
			$nome = $linha['nome'];
			$data_cadastro = $linha['data_criacao'];
			
			$_SESSION['UserName'] = $linha['nome'];
			
			if($linha['foto_perfil'] == ""){
				$tpl->FOTO_PERFIL = '<img src="'.ROOT.'/imagens/avatar.png" alt="'.$nome.'">';
			}else{
				$tpl->FOTO_PERFIL = '<img src="'.ROOT.'/imagens/'.$linha['foto_perfil'].'" alt="'.$nome.'">';
			}
		}

	if($_SESSION['RoleUser'] == '1'){
		$tpl->block("BLOCO_CONFIGURACOES");	
	}
	/*		
	$token_corretora = $_SESSION['TokenCorretora'];
	$query_company = $pdo->query("SELECT background, logo, nome FROM cadastro_empresas WHERE token = '$token_corretora'")->fetchAll();
 		
		foreach($query_company as $ln){
			$nome = $ln['nome'];
			$background = $ln['background'];
			$logo = $ln['logo'];
		}
		
	$tpl->BACKGROUND = $background;
	if($logo != ""){
		$tpl->LOGO_EMPRESA = '<img src="'.ROOT.'/admin/imagens/'.$logo.'" class="img-responsive" alt="'.$nome.'" />';
	}else{
		$tpl->LOGO_EMPRESA = '<img src="'.ROOT.'/src/imagens/logo.png" class="img-responsive" alt="Simulador Group Saúde" />';	
	}
	*/
	
	$query = $pdo->query("SELECT * FROM cadastro_contas_bancarias WHERE token_empresa = '$token' ")->fetchAll();
 		
		foreach($query as $linha){
			$tpl->ID = $linha['id'];
			$tpl->CONTA = $linha['nome_conta'];
			$tpl->SALDO = $linha['saldo_inicial'];
			if($linha['tipo_conta'] == 1){
				$tpl->TIPO = "Conta Corrente";
			}
			if($linha['tipo_conta'] == 2){
				$tpl->TIPO = "Poupança";
			}
			if($linha['tipo_conta'] == 3){
				$tpl->TIPO = "Carteira";
			}
			if($linha['tipo_conta'] == 4){
				$tpl->TIPO = "Caixa";
			}
			if($linha['tipo_conta'] == 5){
				$tpl->TIPO = "Investimento";
			}
			if($linha['tipo_conta'] == 6){
				$tpl->TIPO = "Outros";
			}
			if($linha['tipo_conta'] == 7){
				$tpl->TIPO = "Cartão de Crédito";
			}
			
			if($linha['conta_principal'] == 1){
				$tpl->PRINCIPAL = "checked";
			}else{
				$tpl->PRINCIPAL = "";	
			}
			$tpl->block("BLOCO_LISTAGEM");	
		}
		
	$tpl->ROOT = ROOT;
		
    $tpl->DATA = date('Y');
	
	$tpl->ACTION_DELETE = "conta_bancaria";
	$tpl->NOME_USUARIO = $_SESSION['NameUser'];
	$tpl->MENU4 = "nav-active active";
	$tpl->MENU4_4 = "active";
    $tpl->show();

?>