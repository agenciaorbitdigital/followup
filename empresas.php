<?php

	require 'modulos/session-login.php';

	require 'modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/empresas.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("TOPBAR", "templates/topbar.html");

	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SIDEBAR", "templates/sidebar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("FOOTER", "templates/footer.html");
	
	
	//Dados Usuário
	$email_login = $_SESSION['email_login'];
	$token_user = $_SESSION['TokenUser'];

	$tpl->THEME = $_SESSION['THMsystem'];
	$tpl->BACKGROUND = $_SESSION['BGsystem'];
	$tpl->COLOR = $_SESSION['CLRsystem'];

	$query_usuario = $pdo->query("SELECT id, nome, data_criacao, foto_perfil FROM cadastro_usuarios WHERE email = '$email_login'")->fetchAll();
 		
		foreach($query_usuario as $linha){
			$id_usuario = $linha['id'];
			$nome = $linha['nome'];
			$data_cadastro = $linha['data_criacao'];
			
			$_SESSION['UserName'] = $linha['nome'];
			
			if($linha['foto_perfil'] == ""){
				$tpl->FOTO_PERFIL = '<img src="'.ROOT.'/imagens/avatar.png" alt="'.$nome.'">';
			}else{
				$tpl->FOTO_PERFIL = '<img src="'.ROOT.'/imagens/'.$linha['foto_perfil'].'" alt="'.$nome.'">';
			}
		}

	if($_SESSION['RoleUser'] == '1'){
		$tpl->block("BLOCO_CONFIGURACOES");	
	}
	/*		
	$token_corretora = $_SESSION['TokenCorretora'];
	$query_company = $pdo->query("SELECT background, logo, nome FROM cadastro_empresas WHERE token = '$token_corretora'")->fetchAll();
 		
		foreach($query_company as $ln){
			$nome = $ln['nome'];
			$background = $ln['background'];
			$logo = $ln['logo'];
		}
		
	$tpl->BACKGROUND = $background;
	if($logo != ""){
		$tpl->LOGO_EMPRESA = '<img src="'.ROOT.'/admin/imagens/'.$logo.'" class="img-responsive" alt="'.$nome.'" />';
	}else{
		$tpl->LOGO_EMPRESA = '<img src="'.ROOT.'/src/imagens/logo.png" class="img-responsive" alt="Simulador Group Saúde" />';	
	}
	*/
	
		
	$query_empresas = $pdo->query("SELECT * FROM cadastro_empresas WHERE token_admin = '$token_user' AND status = '1'")->fetchAll();
 		
		foreach($query_empresas as $linha){
			$tokenEmpresa = $linha['token'];
			$tpl->ID_EMPRESA = $linha['id'];
			$tpl->EMPRESAS = $linha['fantasia'];
			$tpl->TOKEN = $linha['token'];
				$tpl->RAZAO = $linha['nome'];
				$tpl->CNPJ = $linha['cnpj'];
				$tpl->INSCRICAO = $linha['insc_est'];
				$tpl->TELEFONE = $linha['telefone'];
				$tpl->ENDERECO = $linha['endereco'];
				$tpl->NUMERO = $linha['numero'];
				$tpl->BAIRRO = $linha['bairro'];
				$tpl->CEP = $linha['cep'];
				$tpl->CIDADE = $linha['cidade'];
				$tpl->ESTADO = $linha['estado'];
				$tpl->COMPLEMENTO = $linha['complemento'];
				
				$tpl->IMAGEM_DESTACADA = $linha['logo'];
			
			$query_principal = $pdo->query("SELECT * FROM assoc_usuarios_empresas WHERE token_usuario = '$token_user'")->fetchAll();
 		
			foreach($query_principal as $row){
				
				if($tokenEmpresa === $row['empresa_principal']){
					$tpl->PRINCIPAL_TIT = 'active';
					$tpl->PRINCIPAL_CONT = 'active in';
					$tpl->PRINCIPAL = '(Principal)';
					
				}else{
					$tpl->PRINCIPAL_TIT = '';
					$tpl->PRINCIPAL_CONT = '';
					$tpl->PRINCIPAL = '';
				}	
			}
			
			$tpl->block("BLOCO_EMPRESAS");	
			$tpl->block("BLOCO_EMPRESAS2");	
		}

	$tpl->ROOT = ROOT;
		
    $tpl->DATA = date('Y');
	
	$tpl->ACTION_DELETE = "empresa";
	$tpl->NOME_USUARIO = $_SESSION['NameUser'];

    $tpl->show();

?>