<?php

	require 'modulos/session-login.php';

	require 'modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/categorias.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("TOPBAR", "templates/topbar.html");

	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SIDEBAR", "templates/sidebar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("FOOTER", "templates/footer.html");
	
	
	//Dados Usuário
	$email_login = $_SESSION['email_login'];
	$token = $_SESSION['TokenCorretora'];
					
	$tpl->THEME = $_SESSION['THMsystem'];
	$tpl->BACKGROUND = $_SESSION['BGsystem'];
	$tpl->COLOR = $_SESSION['CLRsystem'];

	$query_usuario = $pdo->query("SELECT id, nome, data_criacao, foto_perfil FROM cadastro_usuarios WHERE email = '$email_login'")->fetchAll();
 		
		foreach($query_usuario as $linha){
			$id_usuario = $linha['id'];
			$nome = $linha['nome'];
			$data_cadastro = $linha['data_criacao'];
			
			$_SESSION['UserName'] = $linha['nome'];
			
			if($linha['foto_perfil'] == ""){
				$tpl->FOTO_PERFIL = '<img src="'.ROOT.'/imagens/avatar.png" alt="'.$nome.'">';
			}else{
				$tpl->FOTO_PERFIL = '<img src="'.ROOT.'/imagens/'.$linha['foto_perfil'].'" alt="'.$nome.'">';
			}
		}

	if($_SESSION['RoleUser'] == '1'){
		$tpl->block("BLOCO_CONFIGURACOES");	
	}
	/*		
	$token_corretora = $_SESSION['TokenCorretora'];
	$query_company = $pdo->query("SELECT background, logo, nome FROM cadastro_empresas WHERE token = '$token_corretora'")->fetchAll();
 		
		foreach($query_company as $ln){
			$nome = $ln['nome'];
			$background = $ln['background'];
			$logo = $ln['logo'];
		}
		
	$tpl->BACKGROUND = $background;
	if($logo != ""){
		$tpl->LOGO_EMPRESA = '<img src="'.ROOT.'/admin/imagens/'.$logo.'" class="img-responsive" alt="'.$nome.'" />';
	}else{
		$tpl->LOGO_EMPRESA = '<img src="'.ROOT.'/src/imagens/logo.png" class="img-responsive" alt="Simulador Group Saúde" />';	
	}
	*/
	
	$query = $pdo->query("SELECT * FROM cadastro_categorias WHERE token_empresa = '$token' AND tipo = '1' ")->fetchAll();
 		
		foreach($query as $linha){
			$tpl->ID1 = $linha['id'];
			$tpl->CATEGORIA1 = $linha['categoria'];
			
			$tpl->block("BLOCO_LISTAGEM1");	
		}
		
	$query2 = $pdo->query("SELECT * FROM cadastro_categorias WHERE token_empresa = '$token' AND tipo = '2' ")->fetchAll();
 		
		foreach($query2 as $linha){
			$tpl->ID2 = $linha['id'];
			$tpl->CATEGORIA2 = $linha['categoria'];
			
			$tpl->block("BLOCO_LISTAGEM2");	
		}
		
	$query3 = $pdo->query("SELECT * FROM cadastro_categorias WHERE token_empresa = '$token' AND tipo = '3' ")->fetchAll();
 		
		foreach($query3 as $linha){
			$tpl->ID3 = $linha['id'];
			$tpl->CATEGORIA3 = $linha['categoria'];
			
			$tpl->block("BLOCO_LISTAGEM3");	
		}
		
	$query4 = $pdo->query("SELECT * FROM cadastro_categorias WHERE token_empresa = '$token' AND tipo = '4' ")->fetchAll();
 		
		foreach($query4 as $linha){
			$tpl->ID4 = $linha['id'];
			$tpl->CATEGORIA4 = $linha['categoria'];
			
			$tpl->block("BLOCO_LISTAGEM4");	
		}
		
	$query5 = $pdo->query("SELECT * FROM cadastro_categorias WHERE token_empresa = '$token' AND tipo = '5' ")->fetchAll();
 		
		foreach($query5 as $linha){
			$tpl->ID5 = $linha['id'];
			$tpl->CATEGORIA5 = $linha['categoria'];
			
			$tpl->block("BLOCO_LISTAGEM5");	
		}
	
if(isset($_GET['active'])){
	$active = $_GET['active'];
	
	if($active == "recebimentos"){
		$tpl->ACTIVE1 = "active";
	}else{
		$tpl->ACTIVE1 = "";
	}
	
	if($active == "fixas"){
		$tpl->ACTIVE2 = "active";
	}else{
		$tpl->ACTIVE2 = "";
	}
	
	if($active == "variaveis"){
		$tpl->ACTIVE3 = "active";
	}else{
		$tpl->ACTIVE3 = "";
	}
	
	if($active == "pessoas"){
		$tpl->ACTIVE4 = "active";
	}else{
		$tpl->ACTIVE4 = "";
	}
	
	if($active == "impostos"){
		$tpl->ACTIVE5 = "active";
	}else{
		$tpl->ACTIVE5 = "";
	}

}
	$tpl->ROOT = ROOT;
		
    $tpl->DATA = date('Y');
	
	$tpl->ACTION_DELETE = "categoria";
	$tpl->NOME_USUARIO = $_SESSION['NameUser'];
	$tpl->MENU4 = "nav-active active";
	$tpl->MENU4_1 = "active";
    $tpl->show();

?>