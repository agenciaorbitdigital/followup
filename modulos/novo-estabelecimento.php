﻿<?php
  $id_estabelecimento = uniqid();
  
  $insert_estabelecimento = $pdo->query("INSERT INTO cadastro_estabelecimentos (id_admin, id_estabelecimento) VALUES ('$id_usuario', '$id_estabelecimento')");
  
  if($insert_estabelecimento){
	  
	 $create_tables = $pdo->query(" 
	 
		SET SQL_MODE = \"NO_AUTO_VALUE_ON_ZERO\";
		SET time_zone = \"+00:00\";
		
		
		/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
		/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
		/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
		/*!40101 SET NAMES utf8mb4 */;
		
		CREATE TABLE `clientes_$id_estabelecimento` (
		  `id` int(11) NOT NULL,
		  `codigo_cliente` char(20) NOT NULL,
		  `nome_cliente` varchar(255) NOT NULL,
		  `telefone` char(15) NOT NULL,
		  `status` char(10) NOT NULL
		) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Cadastro de Clientes';
		
		
		ALTER TABLE `clientes_$id_estabelecimento`
		  ADD PRIMARY KEY (`id`);
		
		ALTER TABLE `clientes_$id_estabelecimento`
		  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
		/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
		/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
		/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
		
		-- ---------------------------------------------------------------------------------------
		
		CREATE TABLE `pedidos_$id_estabelecimento` (
		  `id` int(11) NOT NULL,
		  `numero_pedido` char(20) NOT NULL,
		  `local_pedido` char(20) NOT NULL,
		  `data_pedido` char(20) NOT NULL,
		  `valor_total` char(10) NOT NULL,
		  `status` char(20) NOT NULL
		) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Cadastro de Pedidos';
		
		ALTER TABLE `pedidos_$id_estabelecimento`
		  ADD PRIMARY KEY (`id`);
		
		ALTER TABLE `pedidos_$id_estabelecimento`
		  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
		/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
		/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
		/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
		
		-- ---------------------------------------------------------------------------------------
	 	
		CREATE TABLE `produtos_$id_estabelecimento` (
		  `id` int(11) NOT NULL,
		  `codigo_produto` char(20) NOT NULL,
		  `nome_produto` varchar(255) NOT NULL,
		  `descricao_curta` varchar(255) NOT NULL,
		  `imagem_produto` varchar(100) NOT NULL,
		  `preco_de` char(20) NOT NULL,
		  `preco_por` char(20) NOT NULL,
		  `status` char(15) NOT NULL
		) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Cadastro de Produtos ';
		
		ALTER TABLE `produtos_$id_estabelecimento`
		  ADD PRIMARY KEY (`id`);
		
		ALTER TABLE `produtos_$id_estabelecimento`
		  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
		/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
		/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
		/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

	 ");
	 if($create_tables){
	 	echo "<script>window.location.href = '../dados-estabelecimento.php?id=".$id_estabelecimento."&adm=".$id_usuario."'</script>";
	 }
	  
  }else{
	
	print_r($pdo->errorInfo());
			  
  }
?>