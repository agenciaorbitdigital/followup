<?php session_start();
header('Content-Type: text/html; charset=utf-8');

	require 'connection-db.php';
	
if(isset($_POST['email_login']) && ($_POST['password'])){
	
	$email_login = $_POST['email_login'];
	$password = $_POST['password'];
	
	$custo = '08';
	$salt = 'Cf1f11ePArKlBJomM0F6aJ';
			
	// Gera um hash baseado em bcrypt
	$hash = crypt($password, '$2a$' . $custo . '$' . $salt . '$');
	
	$query_id = $pdo->query("SELECT id, token, nome, status, hash, role, token_empresa FROM cadastro_usuarios WHERE email = '$email_login'")->fetchAll();
 		
		foreach($query_id as $linha){
			$id_usuario = $linha['id'];
			$status = $linha['status'];
			$hash_bd = $linha['hash'];
			$role = $linha['role'];
			$nome = $linha['nome'];
			$token_user = $linha['token'];
		}
		
	$query_company_p = $pdo->query("SELECT empresa_principal FROM assoc_usuarios_empresas WHERE token_usuario = '$token_user'")->fetchAll();
 	
		foreach($query_company_p as $row){
			
			$token_empresa = $row['empresa_principal'];
		}
	
	$query_company = $pdo->query("SELECT id, status, token, background, theme, color, logo FROM cadastro_empresas WHERE token = '$token_empresa'")->fetchAll();
 		
		foreach($query_company as $ln){
			$status_company = $ln['status'];
			$background = $ln['background'];
			$color = $ln['color'];
			$theme = $ln['theme'];
			$logo = $ln['logo'];
		}
		
		$count = count($query_id);
		
		if($count == 0){
			echo "<script>alert('Usuário não cadastrado!'); window.location.href = '../login'</script>>";
		}else{
		
		if($hash == $hash_bd){
			
			if($status == '1' AND $status_company == '1'){
				
					$_SESSION['UserID'] = $id_usuario;
					$_SESSION['UserAndPass'] = "true";
					$_SESSION['email_login'] = $email_login;
					$_SESSION['senha_login'] = $hash;
					$_SESSION['RoleUser'] = $role;
					$_SESSION['NameUser'] = $nome;
					$_SESSION['TokenCorretora'] = $token_empresa;
				    $_SESSION['TokenUser'] = $token_user;
				    $_SESSION['BGsystem'] = $background;
				    $_SESSION['CLRsystem'] = $color;
					$_SESSION['THMsystem'] = $theme;
					
					echo "<script>window.location.href = '../dashboard'</script>>";
		
			}else{
				echo "<script>alert('Você não possui permissão para acessar o dashboard! Entre em contato!'); window.location.href = '../login'</script>>";
			}

		}
		if($hash !== $hash_bd){
			echo "<script>alert('As senhas digitadas não conferem!'); window.location.href = '../login'</script>>";
		}
		
		}
}
?>