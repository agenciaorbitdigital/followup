﻿<?php
setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
date_default_timezone_set('America/Sao_Paulo');

$root = dirname( $_SERVER["PHP_SELF"] ) == DIRECTORY_SEPARATOR ? "" : dirname( $_SERVER["PHP_SELF"] );
define( "ROOT", $root );

if ( dirname( $_SERVER["PHP_SELF"] ) == DIRECTORY_SEPARATOR ) {
    $root = "";
} else {
    $root = dirname( $_SERVER["PHP_SELF"] );
}

// PDO
$pdo = new PDO("mysql:host=localhost;dbname=followup", 'followup', 'af4cxsA8g0zumPKD');
// define para que o PDO lance exceções caso ocorra erros
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
$pdo->exec("SET CHARACTER SET utf8");
?>