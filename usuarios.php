<?php

	require 'modulos/session-login.php';

	require 'modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/usuarios.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("TOPBAR", "templates/topbar.html");

	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SIDEBAR", "templates/sidebar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("FOOTER", "templates/footer.html");
	
	
	//Dados Usuário
	$email_login = $_SESSION['email_login'];
	$token_user = $_SESSION['TokenUser'];
$token_empresa = $_SESSION['TokenCorretora'];

	$tpl->THEME = $_SESSION['THMsystem'];
	$tpl->BACKGROUND = $_SESSION['BGsystem'];
	$tpl->COLOR = $_SESSION['CLRsystem'];

	$query_usuario = $pdo->query("SELECT id, nome, data_criacao, foto_perfil FROM cadastro_usuarios WHERE email = '$email_login'")->fetchAll();
 		
		foreach($query_usuario as $linha){
			$id_usuario = $linha['id'];
			$nome = $linha['nome'];
			$data_cadastro = $linha['data_criacao'];
			
			$_SESSION['UserName'] = $linha['nome'];
			
			if($linha['foto_perfil'] == ""){
				$tpl->FOTO_PERFIL = '<img src="'.ROOT.'/imagens/avatar.png" alt="'.$nome.'">';
			}else{
				$tpl->FOTO_PERFIL = '<img src="'.ROOT.'/imagens/'.$linha['foto_perfil'].'" alt="'.$nome.'">';
			}
		}

	if($_SESSION['RoleUser'] == '1'){
		$tpl->block("BLOCO_CONFIGURACOES");	
	}
	/*		
	$token_corretora = $_SESSION['TokenCorretora'];
	$query_company = $pdo->query("SELECT background, logo, nome FROM cadastro_empresas WHERE token = '$token_corretora'")->fetchAll();
 		
		foreach($query_company as $ln){
			$nome = $ln['nome'];
			$background = $ln['background'];
			$logo = $ln['logo'];
		}
		
	$tpl->BACKGROUND = $background;
	if($logo != ""){
		$tpl->LOGO_EMPRESA = '<img src="'.ROOT.'/admin/imagens/'.$logo.'" class="img-responsive" alt="'.$nome.'" />';
	}else{
		$tpl->LOGO_EMPRESA = '<img src="'.ROOT.'/src/imagens/logo.png" class="img-responsive" alt="Simulador Group Saúde" />';	
	}
	*/
	
		
	$query_usuarios = $pdo->query("SELECT * FROM cadastro_usuarios WHERE token_empresa = '$token_empresa'")->fetchAll();
 		
		foreach($query_usuarios as $linha){
			$TokenUser = $linha['token'];
			$tpl->ID_USUARIO = $linha['id'];
			$tpl->NOMES = $linha['nome'];
			$tpl->TOKEN = $linha['token'];
			$tpl->SOBRENOME = $linha['sobrenome'];
			$tpl->EMAIL = $linha['email'];
			$tpl->CPF = $linha['cpf'];
			$tpl->CELULAR = $linha['celular'];
			$tpl->CARGO = $linha['cargo'];
			
				if($linha['foto_perfil'] == ""){
				$tpl->IMAGEM_DESTACADA = 'avatar.png';
			}else{
				$tpl->IMAGEM_DESTACADA = $linha['foto_perfil'];
			}
				
			$query_principal = $pdo->query("SELECT * FROM cadastro_empresas WHERE token = '$token_empresa'")->fetchAll();
 		
			foreach($query_principal as $row){
				
				if($TokenUser === $row['token_admin']){
					$tpl->PRINCIPAL_TIT = 'active';
					$tpl->PRINCIPAL_CONT = 'active in';
					$tpl->PRINCIPAL = '(Principal)';
					$tpl->DELETE_USUARIO = '';
					$tpl->DISABLED_PRINCIPAL = "disabled";
					$tpl->DISABLED_PRINCIPAL_INPUT = '<input type="hidden" name="email" value="'.$linha['email'].'" />';
				}else{
					$tpl->PRINCIPAL_TIT = '';
					$tpl->PRINCIPAL_CONT = '';
					$tpl->PRINCIPAL = '';
					$tpl->DELETE_USUARIO = '<a data-toggle="modal" data-target="#modal-slideright" class="btn btn-danger excluir" id="'.$linha['id'].'"><i class="fa fa-trash"></i></a>';
					$tpl->DISABLED_PRINCIPAL = "";
					$tpl->DISABLED_PRINCIPAL_INPUT = "";
				}	
			}
			
		
			
			$tpl->block("BLOCO_USUARIOS");	
			$tpl->block("BLOCO_USUARIOS2");	
		}

	$tpl->ROOT = ROOT;
		
    $tpl->DATA = date('Y');
	
	$tpl->ACTION_DELETE = "usuario";
	$tpl->NOME_USUARIO = $_SESSION['NameUser'];

    $tpl->show();

?>