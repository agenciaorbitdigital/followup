-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 06-Fev-2018 às 14:10
-- Versão do servidor: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `followup`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `assoc_transacoes_tags`
--

CREATE TABLE `assoc_transacoes_tags` (
  `id` int(11) NOT NULL,
  `token_transacao` varchar(255) NOT NULL,
  `id_tag` int(11) NOT NULL,
  `data_cadastro` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `assoc_usuarios_empresas`
--

CREATE TABLE `assoc_usuarios_empresas` (
  `id` int(11) NOT NULL,
  `token_usuario` varchar(255) NOT NULL,
  `empresa_principal` varchar(255) NOT NULL,
  `data_cadastro` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `assoc_usuarios_empresas`
--

INSERT INTO `assoc_usuarios_empresas` (`id`, `token_usuario`, `empresa_principal`, `data_cadastro`) VALUES
(1, '874u48jjdhj283674h', '3524d5a6257760de471ba20e3b5252d1', '2018-02-05 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cadastro_cartoes`
--

CREATE TABLE `cadastro_cartoes` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `tipo` int(11) NOT NULL,
  `bandeira` varchar(100) NOT NULL,
  `parcelamento` varchar(100) NOT NULL,
  `taxa` varchar(100) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cadastro_categorias`
--

CREATE TABLE `cadastro_categorias` (
  `id` int(11) NOT NULL,
  `token_empresa` varchar(255) NOT NULL,
  `categoria` varchar(255) NOT NULL,
  `tipo` int(11) NOT NULL,
  `data_cadastro` datetime NOT NULL,
  `cadastrado_por` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cadastro_centros_de_custo`
--

CREATE TABLE `cadastro_centros_de_custo` (
  `id` int(11) NOT NULL,
  `token_empresa` varchar(255) NOT NULL,
  `centro_de_custo` varchar(255) NOT NULL,
  `data_cadastro` datetime NOT NULL,
  `cadastrado_por` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cadastro_clientes`
--

CREATE TABLE `cadastro_clientes` (
  `id` int(11) NOT NULL,
  `token_empresa` varchar(255) NOT NULL,
  `tipo_cadastro` int(11) NOT NULL,
  `categoria` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `cpf` varchar(20) NOT NULL,
  `cnpj` varchar(20) NOT NULL,
  `insc_estadual` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `telefone` varchar(20) NOT NULL,
  `telefone2` varchar(20) NOT NULL,
  `nome_contato` varchar(255) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `numero` varchar(10) NOT NULL,
  `complemento` varchar(30) NOT NULL,
  `bairro` varchar(255) NOT NULL,
  `cep` varchar(30) NOT NULL,
  `estado` varchar(255) NOT NULL,
  `cidade` varchar(255) NOT NULL,
  `descricao` longtext NOT NULL,
  `data_cadastro` datetime NOT NULL,
  `cadastrado_por` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cadastro_clientes`
--

INSERT INTO `cadastro_clientes` (`id`, `token_empresa`, `tipo_cadastro`, `categoria`, `nome`, `cpf`, `cnpj`, `insc_estadual`, `email`, `telefone`, `telefone2`, `nome_contato`, `endereco`, `numero`, `complemento`, `bairro`, `cep`, `estado`, `cidade`, `descricao`, `data_cadastro`, `cadastrado_por`) VALUES
(1, '3524d5a6257760de471ba20e3b5252d1', 1, 1, 'Teste', '', '', '', 'webmaster@orbitdigital.com.br', '', '', '', '', '', '', '', '', '', '', '', '2018-01-31 15:01:45', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cadastro_contas_bancarias`
--

CREATE TABLE `cadastro_contas_bancarias` (
  `id` int(11) NOT NULL,
  `token_empresa` varchar(255) NOT NULL,
  `conta_principal` int(11) NOT NULL,
  `nome_conta` varchar(255) NOT NULL,
  `tipo_conta` int(11) NOT NULL,
  `saldo_inicial` varchar(50) NOT NULL,
  `saldo_atual` varchar(100) NOT NULL,
  `data_cadastro` datetime NOT NULL,
  `cadastrado_por` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cadastro_empresas`
--

CREATE TABLE `cadastro_empresas` (
  `id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `fantasia` varchar(255) NOT NULL,
  `cnpj` varchar(50) NOT NULL,
  `insc_est` varchar(50) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `background` longtext NOT NULL,
  `theme` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `numero` varchar(255) NOT NULL,
  `complemento` varchar(255) NOT NULL,
  `bairro` varchar(255) NOT NULL,
  `cidade` varchar(255) NOT NULL,
  `estado` varchar(10) NOT NULL,
  `cep` varchar(30) NOT NULL,
  `telefone` varchar(255) NOT NULL,
  `data_cadastro` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `token_admin` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cadastro_empresas`
--

INSERT INTO `cadastro_empresas` (`id`, `token`, `nome`, `fantasia`, `cnpj`, `insc_est`, `logo`, `background`, `theme`, `color`, `endereco`, `numero`, `complemento`, `bairro`, `cidade`, `estado`, `cep`, `telefone`, `data_cadastro`, `status`, `slug`, `token_admin`) VALUES
(1, '3524d5a6257760de471ba20e3b5252d1', '', 'FollowUp', '78436476574', '2345454', '3389185a1e3ef7c6fc3.jpg', 'bg-clean', 'theme-sdtd', 'color-default', 'Rua Baião Parente', '365', '23', 'Vila Primavera', 'São Paulo', 'SP', '02735-000', '(11) 3977-3674', '2017-10-26 14:10:37', 1, 'follow-up', '874u48jjdhj283674h'),
(2, 'a41badcefc07f712270d36e31a0523c7', 'tete', 'wertyre', '12.345.678/6543-23', '234567', '8737235a74c21b252c8.jpg', '', 'theme-sdtl', 'color-default', 'Rua 21-A (Cj João Sampaio I)', '123456', '234567', 'Petrópolis', 'Maceió', 'AL', '21345-654', '(12) 3456-76543', '2018-02-02 15:02:14', 1, 'wertyre', ''),
(3, '9089787hna6257760de471baierjrb5252d1', 'Orbit', 'Orbit Digital', '', '', '4371175a749e464afad.png', 'bg-clean', 'theme-sdtd', 'color-default', 'Rua Baião Parente', '365', '', 'Vila Primavera', 'São Paulo', 'SP', '02735-000', '(11) 3977-3674', '2017-10-26 14:10:37', 1, 'follow-up', '874u48jjdhj283674h');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cadastro_tags`
--

CREATE TABLE `cadastro_tags` (
  `id` int(11) NOT NULL,
  `token_empresa` varchar(255) NOT NULL,
  `tag` varchar(255) NOT NULL,
  `data_cadastro` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cadastro_transacoes`
--

CREATE TABLE `cadastro_transacoes` (
  `id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `token_empresa` varchar(255) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `valor` varchar(50) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `id_forma_de_pagamento` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `data_cadastro` varchar(100) NOT NULL,
  `num_documento` varchar(100) NOT NULL,
  `comprovante` varchar(255) NOT NULL,
  `id_centro_de_custo` int(11) NOT NULL,
  `id_modo_de_pagamento` int(11) NOT NULL,
  `comentarios` longtext NOT NULL,
  `cadastrado_por` int(11) NOT NULL,
  `dia_competencia` int(10) NOT NULL,
  `mes_competencia` int(10) NOT NULL,
  `ano_competencia` int(10) NOT NULL,
  `dia_transacao` int(10) NOT NULL,
  `mes_transacao` int(10) NOT NULL,
  `ano_transacao` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cadastro_usuarios`
--

CREATE TABLE `cadastro_usuarios` (
  `id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `cpf` varchar(255) NOT NULL,
  `sobrenome` varchar(255) NOT NULL,
  `foto_perfil` varchar(255) NOT NULL,
  `celular` varchar(20) NOT NULL,
  `cargo` varchar(255) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `data_criacao` datetime NOT NULL,
  `role` int(11) NOT NULL,
  `status` char(12) NOT NULL,
  `token_empresa` varchar(255) NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cadastro_usuarios`
--

INSERT INTO `cadastro_usuarios` (`id`, `token`, `email`, `nome`, `cpf`, `sobrenome`, `foto_perfil`, `celular`, `cargo`, `hash`, `data_criacao`, `role`, `status`, `token_empresa`, `admin`) VALUES
(1, '874u48jjdhj283674h', 'webmaster@orbitdigital.com.br', 'Administrador', '87990879', 'Orbit', '9664955a79ae103a647.png', '(11) 9858-50671', 'Teste', '$2a$08$Cf1f11ePArKlBJomM0F6a.v6XwVei/xHynLwD5oHISriGW8uRyzRq', '2017-09-04 00:00:00', 1, '1', '3524d5a6257760de471ba20e3b5252d1', 1),
(7, '', '', '', '', '', '', '(11) 9858-50671', '', '$2a$08$Cf1f11ePArKlBJomM0F6a.UFZ6Sp2bbz/FEWdXSFF6hx71tGrjUc.', '2018-01-31 17:01:35', 2, '0', '1', 0),
(10, '54cffd87d0a73b044faba84915e7c7ce', 'felipe@orbitdigital.com', 'Felipe', '422.453.308-11', 'Rosseti', '', '(11) 94793-8517', 'Diretor', '$2a$08$Cf1f11ePArKlBJomM0F6a.j6/zWYbxa1beIDqDjaBsd6mH3A8dXHq', '2018-02-02 13:02:25', 1, '1', 'fdaa902ba01f5257bb7e40b6d41218e5', 0),
(11, '6c316e144a5a79b5b9123cd107c5966a', 'Feli@or.com', 'Ultimo', '098.789.009-87', 'Teste', '', '(09) 87890-0987', '0987890', '$2a$08$Cf1f11ePArKlBJomM0F6a.UFZ6Sp2bbz/FEWdXSFF6hx71tGrjUc.', '2018-02-02 14:02:18', 1, '1', '103fa8585aba18b6e5cea04373b141fd', 0),
(12, '22432a3b6c6630461cef4836f8f22763', 'felipe@orbit.com.gr', 'Felipe ', '123.876.543-45', 'rosseti', '', '(09) 87656-7890', '09876567890', '$2a$08$Cf1f11ePArKlBJomM0F6a.UFZ6Sp2bbz/FEWdXSFF6hx71tGrjUc.', '2018-02-02 14:02:05', 1, '1', '6ebdae252df2bbd5d9e8023328e1ba1d', 0),
(14, '', '', '', '', '', '', '', '', '$2a$08$Cf1f11ePArKlBJomM0F6a.doi9KzmOLeThS0MTQyqmCCXt6P.E3.a', '2018-02-02 15:02:37', 1, '0', '', 0),
(15, 'f97f6e4f206b62cdd5742e21763120b4', '3454@fsgdh.com', '324564', '324.564.323-45', '345643', '', '(34) 56432-4564', '345', '$2a$08$Cf1f11ePArKlBJomM0F6a.UFZ6Sp2bbz/FEWdXSFF6hx71tGrjUc.', '2018-02-02 15:02:47', 1, '1', '8ed15064649c587d3fb0bbdc8b448d4c', 0),
(16, '0a3ef0d774079bcc237a337c193e6ea7', 'etet@retet.com', 'tete', '234.565.432-45', 'etet', '', '(34) 56754-3245', '3456432', '$2a$08$Cf1f11ePArKlBJomM0F6a.UFZ6Sp2bbz/FEWdXSFF6hx71tGrjUc.', '2018-02-02 15:02:14', 1, '1', 'a41badcefc07f712270d36e31a0523c7', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assoc_transacoes_tags`
--
ALTER TABLE `assoc_transacoes_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assoc_usuarios_empresas`
--
ALTER TABLE `assoc_usuarios_empresas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cadastro_cartoes`
--
ALTER TABLE `cadastro_cartoes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cadastro_categorias`
--
ALTER TABLE `cadastro_categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cadastro_centros_de_custo`
--
ALTER TABLE `cadastro_centros_de_custo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cadastro_clientes`
--
ALTER TABLE `cadastro_clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cadastro_contas_bancarias`
--
ALTER TABLE `cadastro_contas_bancarias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cadastro_empresas`
--
ALTER TABLE `cadastro_empresas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cadastro_tags`
--
ALTER TABLE `cadastro_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cadastro_transacoes`
--
ALTER TABLE `cadastro_transacoes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cadastro_usuarios`
--
ALTER TABLE `cadastro_usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assoc_transacoes_tags`
--
ALTER TABLE `assoc_transacoes_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `assoc_usuarios_empresas`
--
ALTER TABLE `assoc_usuarios_empresas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cadastro_cartoes`
--
ALTER TABLE `cadastro_cartoes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cadastro_categorias`
--
ALTER TABLE `cadastro_categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cadastro_centros_de_custo`
--
ALTER TABLE `cadastro_centros_de_custo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cadastro_clientes`
--
ALTER TABLE `cadastro_clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cadastro_contas_bancarias`
--
ALTER TABLE `cadastro_contas_bancarias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cadastro_empresas`
--
ALTER TABLE `cadastro_empresas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `cadastro_tags`
--
ALTER TABLE `cadastro_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cadastro_transacoes`
--
ALTER TABLE `cadastro_transacoes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cadastro_usuarios`
--
ALTER TABLE `cadastro_usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
