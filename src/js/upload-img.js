
$(document).ready(function (e) {
	
	$("#upload_logo").on('submit',(function(e) {
		e.preventDefault();
		$.ajax({
        	url: "modulos/upload-logo.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			beforeSend : function()
			{
				//$("#preview").fadeOut();
				$("#err").fadeOut();
			},
			success: function(data)
		    {
				if(data == 'invalid')
				{
					// invalid file format.
					$("#err").html("Arquivo Invalido!").fadeIn();
				}
				else
				{
					// view uploaded file.
					$("#preview").html(data).fadeIn();
					$("#upload_logo")[0].reset();	
					
					$("#btn_finalizar").show("slow");
				}
		    },
		  	error: function(e) 
	    	{
				$("#err").html(e).fadeIn();
	    	} 	        
	   });
	}));
	

});

