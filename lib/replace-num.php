﻿<?php
function valor_func($aValor, $aTipo, $a_dec = 2) { // valores com mascara e sem mascara
   switch ($aTipo):
      case 'C':// com mascara
         $valor = number_format(round($aValor, $a_dec), $a_dec, ',', '.');
         break;

      case 'S':// sem mascara
         $valor = str_replace(',', '.', str_replace('.', '', $aValor));
         break;

      case 'A':// arrendonda
         $valor = round($aValor, $a_dec);
         break;
      case 'D':// Decimais sem arredonda,sem mascara
         $posPonto = strpos($aValor, '.');
         if ($posPonto > 0):
            $valor = substr($aValor, 0, $posPonto) . '.' . substr($aValor, $posPonto + 1, $a_dec);
         else:
            $valor = $aValor;
         endif;
         break;
   endswitch;
   return $valor;
}
?>