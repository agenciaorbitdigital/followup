<?php

	require 'modulos/session-login.php';

	require '../modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/empresas.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("TOPBAR", "templates/topbar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SIDEBAR", "templates/sidebar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("FOOTER", "templates/footer.html");
	
	
	//Dados Usuário
	$email_login = $_SESSION['email_login'];
	
	$query_usuario = $pdo->query("SELECT id, nome, data_criacao FROM cadastro_usuarios WHERE email = '$email_login'")->fetchAll();
 		
		foreach($query_usuario as $linha){
			$id_usuario = $linha['id'];
			$nome = $linha['nome'];
			$data_cadastro = $linha['data_criacao'];
			
			$_SESSION['UserName'] = $linha['nome'];
		}
	
	$query_corretoras = $pdo->query("SELECT id, nome, status, slug FROM cadastro_empresas ORDER BY id DESC")->fetchAll();
			
			foreach($query_corretoras as $linha){
				$tpl->ID = $linha['id'];
				$tpl->NOME = $linha['nome'];
				$tpl->SLUG = $linha['slug'];
				if($linha['status'] == 1){
					$tpl->STATUS = "Ativo";
				}if($linha['status'] == 0){
					$tpl->STATUS = "Inativo";
				}
				
				$tpl->block("BLOCO_LISTAGEM");
			}
			
	$tpl->ROOT = ROOT;
		
    $tpl->DATA = date('Y');
	
	$tpl->NOME_USUARIO = $_SESSION['NameUser'];
	$tpl->MENU2 = "active";
	$tpl->MENU2_1 = "active";
    $tpl->show();

?>