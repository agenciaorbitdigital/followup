<?php

	require 'modulos/session-login.php';

	require '../modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/usuarios.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("TOPBAR", "templates/topbar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SIDEBAR", "templates/sidebar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("DASH_INFO", "templates/dash-info.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("FOOTER", "templates/footer.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	//Dados Usuário
	$email_login = $_SESSION['email_login'];
	$userName = $_SESSION['UserName'];
	
	
	if(isset($_GET['filtro']) && $_GET['filtro'] == 'ativos'){
		$tpl->FILTRO = "(Usuários Ativos)";
		
		$query_usuario = $pdo->query("SELECT * FROM cadastro_usuarios WHERE status = '1' AND role = '1' ORDER BY id DESC")->fetchAll();
			
			foreach($query_usuario as $linha){
				$tpl->ID_USUARIO = $linha['id'];
				$tpl->NOME = $linha['nome'];
				$tpl->EMAIL = $linha['email'];
				$tpl->DATA_CRIACAO = $linha['data_criacao'];
				if($linha['status'] == 1){
					$tpl->STATUS = "Ativo";
				}if($linha['status'] == 0){
					$tpl->STATUS = "Inativo";
				}
				
				if($linha['role'] == 1){
					$tpl->ROLE = "Administrador";
				}if($linha['role'] == 2){
					$tpl->ROLE = "Cliente";
				}
				
				$tpl->block("BLOCO_CLIENTES");
			}
	}
	if(isset($_GET['filtro']) && $_GET['filtro'] == 'inativos'){
		$tpl->FILTRO = "(Usuários Inativos)";
		
		$query_usuario = $pdo->query("SELECT * FROM cadastro_usuarios WHERE status = '0' AND role = '1' ORDER BY id DESC")->fetchAll();
			
			foreach($query_usuario as $linha){
				$tpl->ID_USUARIO = $linha['id'];
				$tpl->NOME = $linha['nome'];
				$tpl->EMAIL = $linha['email'];
				$tpl->DATA_CRIACAO = $linha['data_criacao'];
				if($linha['status'] == 1){
					$tpl->STATUS = "Ativo";
				}if($linha['status'] == 0){
					$tpl->STATUS = "Inativo";
				}
				
				if($linha['role'] == 1){
					$tpl->ROLE = "Administrador";
				}if($linha['role'] == 0){
					$tpl->ROLE = "Cliente";
				}
				
				$tpl->block("BLOCO_CLIENTES");
			}
	}
	if(isset($_GET['filtro']) && $_GET['filtro'] == 'mes'){
		$tpl->FILTRO = "(Cadastrados este Mês)";
		
		$query_usuario = $pdo->query("SELECT * FROM cadastro_usuarios WHERE role = '1' AND MONTH(data_criacao) =  MONTH(now()) ORDER BY id DESC")->fetchAll();
			
			foreach($query_usuario as $linha){
				$tpl->ID_USUARIO = $linha['id'];
				$tpl->NOME = $linha['nome'];
				$tpl->EMAIL = $linha['email'];
				$tpl->DATA_CRIACAO = $linha['data_criacao'];
				if($linha['status'] == 1){
					$tpl->STATUS = "Ativo";
				}if($linha['status'] == 0){
					$tpl->STATUS = "Inativo";
				}
				
				if($linha['role'] == 1){
					$tpl->ROLE = "Administrador";
				}if($linha['role'] == 0){
					$tpl->ROLE = "Cliente";
				}
				
				$tpl->block("BLOCO_CLIENTES");
			}
	}
	if(!isset($_GET['filtro'])){
		$tpl->FILTRO = "(Todos)";

		$query_usuario = $pdo->query("SELECT * FROM cadastro_usuarios WHERE role = '1' ORDER BY id DESC")->fetchAll();
			
			foreach($query_usuario as $linha){
				$tpl->ID_USUARIO = $linha['id'];
				$tpl->NOME = $linha['nome'];
				$tpl->EMAIL = $linha['email'];
				$tpl->DATA_CRIACAO = $linha['data_criacao'];
				if($linha['status'] == 1){
					$tpl->STATUS = "Ativo";
				}if($linha['status'] == 0){
					$tpl->STATUS = "Inativo";
				}
				
				if($linha['role'] == 1){
					$tpl->ROLE = "Administrador";
				}if($linha['role'] == 0){
					$tpl->ROLE = "Cliente";
				}
				
				$tpl->block("BLOCO_CLIENTES");
			}
	}
		
    $tpl->DATA = date('Y');
	$tpl->NOME = $userName;
	$tpl->ROOT = ROOT;
	$tpl->MENU5 = "active";
	$tpl->MENU5_1 = "active";
    $tpl->show();

?>