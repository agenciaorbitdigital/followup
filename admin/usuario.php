<?php
	require 'modulos/session-login.php';

	require '../modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/usuario.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("TOPBAR", "templates/topbar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SIDEBAR", "templates/sidebar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("FOOTER", "templates/footer.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	//Dados Usuário
	$email_login = $_SESSION['email_login'];
	
	$tpl->ROOT = ROOT;
	
	$id = $_GET['id'];
	$tpl->ID = $id;
	
	$query_usuario = $pdo->query("SELECT * FROM cadastro_usuarios WHERE id = '$id'")->fetchAll();
 		
		foreach($query_usuario as $linha){
			$token_empresa = $linha['token_empresa'];
			$tpl->TOKEN_USER = $linha['token'];
			$token_user = $linha['token'];
			$tpl->NOME = $linha['nome'];
			$tpl->SOBRENOME = $linha['sobrenome'];
			$tpl->EMAIL = $linha['email'];
			$tpl->CPF = $linha['cpf'];
			$tpl->CELULAR = $linha['celular'];
			$tpl->CARGO = $linha['cargo'];
			$tpl->DATA_CADASTRO = $linha['data_criacao'];
			if($linha['status'] == 1){
				$tpl->STATUS_EXPL = "checked";
				$tpl->STATUS = "Ativo";
			}else{
				$tpl->STATUS_EXPL = "";
				$tpl->STATUS = "Inativo";
			}
			
		}
	
	$query_corretoras = $pdo->query("SELECT * FROM cadastro_empresas WHERE token_admin = '$token_user'")->fetchAll();
			
			foreach($query_corretoras as $row){
				$tpl->ID = $row['id'];
				$tpl->TOKEN = $row['token'];
				$tpl->EMPRESA = $row['nome'];
				$tpl->FANTASIA = $row['fantasia'];
				$tpl->CNPJ = $row['cnpj'];
				$tpl->INSCRICAO = $row['insc_est'];
				$tpl->TELEFONE = $row['telefone'];
				$tpl->ENDERECO = $row['endereco'];
				$tpl->NUMERO = $row['numero'];
				$tpl->BAIRRO = $row['bairro'];
				$tpl->CEP = $row['cep'];
				$tpl->CIDADE = $row['cidade'];
				$tpl->ESTADO = $row['estado'];
				$tpl->COMPLEMENTO = $row['complemento'];
				
				$tpl->IMAGEM_DESTACADA = $row['logo'];
				
				if($row['status'] == 1){
				$tpl->STATUS_EXPL_EMPRESA = "checked";
				}else{
					$tpl->STATUS_EXPL_EMPRESA = "";
				}
				
				$tpl->block("BLOCO_EMPRESAS");	
			}

	$tpl->MENU5 = "active";
	$tpl->NOME_USUARIO = $_SESSION['UserName'];
    $tpl->show();

?>