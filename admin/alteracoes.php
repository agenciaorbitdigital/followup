<?php

	require 'modulos/session-login.php';

	require '../modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/alteracoes.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("TOPBAR", "templates/topbar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SIDEBAR", "templates/sidebar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("DASH_INFO", "templates/dash-info.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("FOOTER", "templates/footer.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	//Dados Usuário
	$email_login = $_SESSION['email_login'];
	$userName = $_SESSION['UserName'];
	


		$query_usuario = $pdo->query("SELECT * FROM alteracao_empresas WHERE status = '1' ORDER BY id DESC")->fetchAll();
			
			foreach($query_usuario as $linha){
				$tpl->ID = $linha['id'];
				$tpl->NOME = $linha['nome'];
				$tpl->TOKEN = $linha['token_empresa'];
				
				if($linha['status'] == 1){
					$tpl->STATUS = "Ativo";
				}if($linha['status'] == 0){
					$tpl->STATUS = "Inativo";
				}
				
				$tpl->block("BLOCO_CLIENTES");
			}
		
    $tpl->DATA = date('Y');
	$tpl->NOME = $userName;
	$tpl->ROOT = ROOT;
	$tpl->MENU5 = "active";
	$tpl->MENU5_3 = "active";
    $tpl->show();

?>