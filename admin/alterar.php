<?php

	require 'modulos/session-login.php';

	require '../modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/alterar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("TOPBAR", "templates/topbar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SIDEBAR", "templates/sidebar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("FOOTER", "templates/footer.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	//Dados Usuário
	$email_login = $_SESSION['email_login'];
	
	$tpl->ROOT = ROOT;
	
	$token_empresa = $_GET['id'];

	$query_corretoras = $pdo->query("SELECT * FROM alteracao_empresas WHERE token_empresa = '$token_empresa'")->fetchAll();
			
			foreach($query_corretoras as $row){
				$tpl->EMPRESA = $row['nome'];
				$tpl->CNPJ = $row['cnpj'];
				$tpl->INSCRICAO = $row['insc_est'];
				$tpl->TELEFONE = $row['telefone'];
				$tpl->ENDERECO = $row['endereco'];
				$tpl->NUMERO = $row['numero'];
				$tpl->BAIRRO = $row['bairro'];
				$tpl->CEP = $row['cep'];
				$tpl->CIDADE = $row['cidade'];
				$tpl->ESTADO = $row['estado'];
				$tpl->COMPLEMENTO = $row['complemento'];
				


			}

	$tpl->MENU5 = "active";
	$tpl->NOME_USUARIO = $_SESSION['UserName'];
    $tpl->show();

?>