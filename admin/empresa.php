<?php

	require 'modulos/session-login.php';

	require '../modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/empresa.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("TOPBAR", "templates/topbar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SIDEBAR", "templates/sidebar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("FOOTER", "templates/footer.html");
	
	
	//Dados Usuário
	$email_login = $_SESSION['email_login'];
	
	$tpl->ROOT = ROOT;
	
	$slug = $_GET['slug'];
		
	$query_corretoras = $pdo->query("SELECT * FROM cadastro_empresas WHERE slug = '$slug'")->fetchAll();
			
			foreach($query_corretoras as $linha){

				$tpl->ID = $linha['id'];
				$tpl->EMPRESA = $linha['nome'];
				$tpl->TELEFONE = $linha['telefone'];
				$tpl->ENDERECO = $linha['endereco'];
				$tpl->NUMERO = $linha['numero'];
				$tpl->BAIRRO = $linha['bairro'];
				$tpl->CEP = $linha['cep'];
				$tpl->CIDADE = $linha['cidade'];
				$tpl->ESTADO = $linha['estado'];
				$tpl->COMPLEMENTO = $linha['complemento'];
				
				$tpl->IMAGEM_DESTACADA = $linha['logo'];
				$tpl->DATA_CADASTRO = $linha['data_cadastro'];
				
				if($linha['status'] == 1){
					$tpl->STATUS_EXPL = "Ativo";
					$tpl->STATUS_CHECK = "checked";
				}else{
					$tpl->STATUS_EXPL = "Inativo";	
					$tpl->STATUS_CHECK = "";
				}

			}
			
	$query_usuarios = $pdo->query("SELECT id, nome, status FROM cadastro_usuarios WHERE status = '1'")->fetchAll();
 		
		foreach($query_usuarios as $row){
			$tpl->USUARIO = $row['nome'];
			$tpl->ID_USUARIO = $row['id'];
			
			if($row['status'] == 1){
					$tpl->STATUS = "";
					$tpl->EXP_STATUS = "";
				}if($row['status'] == 0){
					$tpl->STATUS = "disabled";
					$tpl->EXP_STATUS = "<small>Inativo</small>";
				}
				
			$tpl->block("BLOCO_USUARIOS");
		}
				
	$tpl->NOME_USUARIO = $_SESSION['NameUser'];				
	$tpl->MENU2 = "active";
    $tpl->show();

?>