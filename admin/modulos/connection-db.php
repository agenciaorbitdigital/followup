﻿<?php
// PDO
$pdo = new PDO("mysql:host=localhost;dbname=teste", 'root', '');
// define para que o PDO lance exceções caso ocorra erros
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
$pdo->exec("SET CHARACTER SET utf8");
?>