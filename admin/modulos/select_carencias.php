﻿<?php
require '../../modulos/connection-db.php';

$query_carencia = $pdo->query("SELECT id, nome FROM cadastro_carencias ORDER BY id DESC")->fetchAll();
			
$json = array();
while ($row = $query_carencia->fetch_assoc()) {
  $json[] = array(
    'id' => $row['id'],
    'carencia' => $row['nome'] // Don't you want the name?
  );
}
echo json_encode($json);			
?>