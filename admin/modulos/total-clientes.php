<?php session_start();

require '../../modulos/connection-db.php';

	$query_clientes = $pdo->query("SELECT id FROM cadastro_usuarios WHERE role ='1'")->fetchAll();
	$todos_clientes = count($query_clientes);
	$dados['todos'] = $todos_clientes;
	
	$query_ativos = $pdo->query("SELECT id FROM cadastro_usuarios where status='1' AND  role ='1'")->fetchAll();
	$todos_ativos = count($query_ativos);
	$dados['ativos'] = $todos_ativos;
	
	$query_inativos = $pdo->query("SELECT id FROM cadastro_usuarios where status='0' AND role ='1'")->fetchAll();
	$todos_inativos = count($query_inativos);
	$dados['inativos'] = $todos_inativos;
	
	$query_inativos = $pdo->query("SELECT id FROM cadastro_usuarios WHERE role ='1' AND MONTH(data_criacao) =  MONTH(now()) ")->fetchAll();
	$todos_inativos = count($query_inativos);
	$dados['mes'] = $todos_inativos;
	
	echo json_encode($dados);

?>