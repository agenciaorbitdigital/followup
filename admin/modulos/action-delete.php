﻿<?php session_start();

require '../../modulos/connection-db.php';

if(!isset($_SESSION['UserAndPass'])){
	
}else if(isset($_SESSION['UserID'])){
	$id = $_GET['id'];
	
	if(isset($_GET['action']) && $_GET['action'] == 'delUsuario'){
		if($id === $_SESSION['UserID']){
			echo "<script>alert('Você não pode excluir o usuario com o qual está logado!'); window.location.href='../clientes';</script>";
			
		}else{
			$delete = $pdo->query("DELETE FROM cadastro_usuarios WHERE id = $id");
			if($delete){
				echo "<script>window.location.href='../usuarios'</script>";
			}
		}
	}
	
	if(isset($_GET['action']) && $_GET['action'] == 'delFilme'){
		$delete = $pdo->query("DELETE FROM cadastro_filmes WHERE id = $id");
		if($delete){
			echo "<script>window.location.href='../filmes'</script>";
		}
	}
	
	if(isset($_GET['action']) && $_GET['action'] == 'delSerie'){
		$delete = $pdo->query("DELETE FROM cadastro_series WHERE id = $id");
		if($delete){
			echo "<script>window.location.href='../series'</script>";
		}
	}
	
	if(isset($_GET['action']) && $_GET['action'] == 'delVideo'){
		$countCat = $pdo->query("SELECT id FROM cadastro_filmes WHERE video = $id")->fetchAll();
		$total = count($countCat);
		
		if($total === 0){
			$delete = $pdo->query("DELETE FROM cadastro_videos WHERE id = $id");
			if($delete){
				echo "<script>window.location.href='../biblioteca'</script>";
			}
		}else{
			echo "<script>alert('Você não pode excluir um vídeo atribuído a um filme!');window.location.href='../biblioteca';</script>";
		}
	}
	
	
	if(isset($_GET['action']) && $_GET['action'] == 'delCatFilmes'){
		
		$countCat = $pdo->query("SELECT id FROM cadastro_filmes WHERE categoria = $id")->fetchAll();
		$total = count($countCat);
		
		if($total === 0){
			$delete = $pdo->query("DELETE FROM categorias_filmes WHERE id = $id");
			if($delete){
				echo "<script>window.location.href='../categorias-filmes'</script>";
			}
		}else{
			echo "<script>alert('Você não pode excluir uma categoria atribuída a um filme!');window.location.href='../categorias-filmes';</script>";
		}
	}
	
	if(isset($_GET['action']) && $_GET['action'] == 'delCatSeries'){
		
		$countCat = $pdo->query("SELECT id FROM cadastro_series WHERE categoria = $id")->fetchAll();
		$total = count($countCat);
		
		if($total === 0){
			$delete = $pdo->query("DELETE FROM categorias_series WHERE id = $id");
			if($delete){
				echo "<script>window.location.href='../categorias-series'</script>";
			}
		}else{
			echo "<script>alert('Você não pode excluir uma categoria atribuída a uma serie!');window.location.href='../categorias-series';</script>";
		}
	}
}
?>