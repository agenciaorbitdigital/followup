﻿// JavaScript Document
//Scripts
$(document).ready(function(){
//inicio do documento
		
		//Desabilita autocomplete de todos os formularios
		$('form').attr('autocomplete', 'off');
		
		//Esconde Retorno Erro Login
		$("#return_login").hide();
		
		$("#new_senha_success").hide();
		//AJAX validar Usuario e Senha BD
		$('#form_login').submit(function(){
			
			var dados = $(this).serialize();
				
	            $.ajax({  
	                type: "POST",  
	                url: "modulos/valida-login.php",  
	                data: dados,
					beforeSend: function(){
						$("#load").html("<img src='src/imagens/carregando.gif'>");
					},
					success: function( data ){

						$("#load").html("");
						
						if(data == 10){
							$('#return_login').html("<p>A senha digitada está incorreta!</p>");
							$("#return_login").show('slow');
							alert(data);
							
						}if(data == 11){
							window.location.href = 'dashboard';
							alert(data);
						}if(data == 0){
							$('#return_login').html("<p>Usuário não cadastrado!</p>");
							$("#return_login").show('slow');
							alert(data);
						}
					
					},
					
					
					error: function(){
						$('#return-erro').show('slow');
					}
	            });  

	            return false;    
	    });

		//AJAX recuperar senha
		$('#recuperar_senha').submit(function(){
			
			var dados = $(this).serialize();
				
	            $.ajax({  
	                type: "POST",  
	                url: "modulos/recuperar-senha.php",  
	                data: dados,
					beforeSend: function(){
						$("#load").html("<img src='src/imagens/carregando.gif'>");
					},
					success: function( data ){

						$("#load").html("");

						if(data == 0){
							$('#retorno').html("<p>E-mail não cadastrado!</p>");
							$("#retorno").show('slow');
						}
						if(data == 1){
							window.location.href = 'login';
						}
					},
					
					
					error: function(){
						$('#retorno').show('slow');
					}
	            });  

	            return false;    
	    });		
		
		
		
		//AJAX alterar senha no Banco de Dados
		$('#form_alterar_senha').submit(function(){
			
			var dados = $(this).serialize();
				
	            $.ajax({  
	                type: "POST",  
	                url: "modulos/alterar-senha.php",  
	                data: dados,
					beforeSend: function(){
						$(".load").html("<img src='src/imagens/carregando.gif'>");
					},
					success: function( data ){
						$(".load").html("");
						
						if(data == 1){
							$("#return_atual").html("A senha digitada está errada!");
						}
						if(data == 0){
							$("#return_nova").html("Nunhum dado recebido!");
						}
						if(data == 2){
							$("#return_nova").html("As senhas digitadas não correspondem!");
						}
						if(data == 3){
							$("#new_senha_success").show("slow");
						}
						
					
					},
					error: function(){
						$('#return-erro').show('slow');
					}
	            });  

	            return false;    
	    });
		
		//AJAX add cliente
		$('#add_usuario').submit(function(){
			
			var dados = $(this).serialize();
				
	            $.ajax({  
	                type: "POST",  
	                url: "modulos/add-cliente.php",  
	                data: dados,
					beforeSend: function(){
						$(".load").html("<img src='src/imagens/carregando.gif'>");
					},
					success: function( data ){
						$(".load").html("");
						
						if(data === 1){
							$("#return").html("Usuário cadastrado com sucesso!");
						}
						else if(data === 0){
							$("#return_email").html("Este e-mail já está cadastrado!");
						}
						else if(data === 2){
							$("#return_senha").html("AS SENHAS SÃO DIFERENTES!");
						}
						
					
					},
					error: function(){
						$('#return-erro').show('slow');
					}
	            });  

	            return false;    
	    });
		//Função AJAX buscar cep e retornar endereço completo
		//Mascaras dos inputs
		//Telefone
		$('#telefone')
        .mask("(99) 9999-9999?9")
        .focusout(function (event) {  
            var target, phone, element;  
            target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
            phone = target.value.replace(/\D/g, '');
            element = $(target);  
            element.unmask();  
            if(phone.length > 10) {  
                element.mask("(99) 99999-999?9");  
            } else {  
                element.mask("(99) 9999-9999?9");  
            }  
        });
		$('#telefone2')
        .mask("(99) 9999-9999?9")
        .focusout(function (event) {  
            var target, phone, element;  
            target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
            phone = target.value.replace(/\D/g, '');
            element = $(target);  
            element.unmask();  
            if(phone.length > 10) {  
                element.mask("(99) 99999-999?9");  
            } else {  
                element.mask("(99) 9999-9999?9");  
            }  
        });
		//CPF
		$('#cpf').mask("999.999.999-99");
		//CNPJ
		$('#cnpj').mask("99.999.999/9999-99");
		//CEP
		$('#cep').mask("99999-999");
		
		$('#datepicker').mask("99/99/9999");
		$('#datepicker2').mask("99/99/9999");
		
   		//Função AJAX buscar cep e retornar endereço completo
   		$("#cep").keyup(function(){
           /* Configura a requisição AJAX */

           $.ajax({
                url : 'modulos/busca-cep.php', /* URL que será chamada */ 
                type : 'POST', /* Tipo da requisição */ 
                data: 'cep=' + $('#cep').val(), /* dado que será enviado via POST */
                dataType: 'json', /* Tipo de transmissão */
				beforeSend: function(){
						$("#load_cep").html("<img src='src/imagens/carregando.gif'>");
					},
                success: function(data){
                    if(data.sucesso == 1){
						
						$("#load_cep").html("");
						
						$('#endereco').show("slow");
						
                        $('#rua').val(data.rua);
                        $('#bairro').val(data.bairro);
                        $('#cidade').val(data.cidade);
                        $('#estado').val(data.estado);
                        
					
                    }
                }
           }); 
   			return false;    
   		})
		
		//AJAX add cliente
		$('#add_loja').submit(function(){
			
			var dados = $(this).serialize();
				
	            $.ajax({  
	                type: "POST",  
	                url: "modulos/add-loja.php",  
	                data: dados,
					beforeSend: function(){
						$(".load").html("<img src='src/imagens/carregando.gif'>");
					},
					success: function( data ){
						$(".load").html("");
						
						if(data == 1){
							$("#return").html("Loja cadastrado com sucesso!");
						}
						if(data == 0){
							$("#return").html("Ocorreu um erro! Tente novamente.");
						}
						
					
					},
					error: function(){
						$('#return-erro').show('slow');
					}
	            });  

	            return false;    
	    });
//fim do documento		
});

	  