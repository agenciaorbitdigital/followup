<!doctype html>
<html>
<head>
<meta charset="utf-8">
<base href="{ROOT}">
<!-- Inclusão do Bootstrap -->
<link rel="stylesheet" href="{ROOT}/src/bootstrap/css/bootstrap.css" />
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{ROOT}/src/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{ROOT}/src/css/AdminLTE.min.css">
 <!-- Tema Azul -->
  <link rel="stylesheet" href="{ROOT}/src/css/skins/skin-blue.min.css">
  <link rel="shortcut icon" href="{ROOT}/src/imagens/icone.png">
  <style type="text/css">
  body,td,th {
	font-family: "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
}
  </style>


  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
<title>Recuperar Senha</title>
</head>
<body class="hold-transition lockscreen" style="overflow:hidden">
<!-- Automatic element centering -->
<div class="lockscreen-wrapper">
  <div class="lockscreen-logo">
    <a href="{ROOT}/login"><img src="{ROOT}/src/imagens/icone.png" class="img-responsive" style="max-width:200px; margin:-80px auto 0 auto;"></a>
  </div>
  <!-- User name -->
  <div class="lockscreen-name">Digite seu e-mail para receber a senha</div>

  <!-- START LOCK SCREEN ITEM -->
  <div class="lockscreen-item">
  
    <!-- lockscreen credentials (contains the form) -->
    <form method="post" id="recuperar_senha" >
      <div class="input-group">
        <input type="email" name="email" class="form-control" placeholder="seu@email.com" required />
        <div class="input-group-btn">
          <button type="submit" class="btn"><i class="fa fa-arrow-right text-muted"></i></button>
        </div>
      </div>
    </form>
    <!-- /.lockscreen credentials -->

  </div>
  <div class="lockscreen-footer text-center">
    <div id="load"></div><!-- /#load -->
    <div id="retorno"></div><!-- /#retorno -->
  </div>
  
  
  <div class="lockscreen-footer text-center">
    Copyright &copy; 2017 <b><a href="httpS://orbitdigital.com.br" class="text-black"> Orbit Digital</a></b><br>
    Todos os Direitos Reservados
  </div>
</div>
<!-- /.center -->

<!-- INCLUSÃO DO JQUERY -->
<script type="text/javascript" src="{ROOT}/src/js/jquery.js"></script> 
<!-- INCLUSÃO DO JQUERY.VALIDATE -->
<script type="text/javascript" src="{ROOT}/src/js/jquery.validate.js"></script>
<!-- INCLUSÃO FUNÇOES JAVASCRIPT -->
<script type="text/javascript" src="{ROOT}/src/js/scripts.js"></script>
</body>
</html>
