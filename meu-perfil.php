<?php

	require 'modulos/session-login.php';

	require 'modulos/connection-db.php';

    require_once("lib/raelgc/view/Template.php");
    use raelgc\view\Template;

    $tpl = new Template("templates/meu-perfil.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("TOPBAR", "templates/topbar.html");

	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SIDEBAR", "templates/sidebar.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("SCRIPTS", "templates/scripts.html");
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("ESTILOS", "templates/estilos.html");
	
	
	// Adicionando mais um arquivo HTML
    $tpl->addFile("FOOTER", "templates/footer.html");
	
	
	//Dados Usuário
	$email_login = $_SESSION['email_login'];
	
	$tpl->THEME = $_SESSION['THMsystem'];
	$tpl->BACKGROUND = $_SESSION['BGsystem'];
	$tpl->COLOR = $_SESSION['CLRsystem'];

	$query_usuario = $pdo->query("SELECT * FROM cadastro_usuarios WHERE email = '$email_login'")->fetchAll();
 		
		foreach($query_usuario as $linha){
			$tpl->TOKEN = $linha['token'];
			$nome = $linha['nome'];
			$tpl->NOME = $nome;
			$tpl->SOBRENOME  = $linha['sobrenome'];
			$tpl->EMAIL = $linha['email'];
			$tpl->CELULAR = $linha['celular'];
			$data_mysql = $linha['data_criacao'];
			$timestamp = strtotime($data_mysql); // Gera o timestamp de $data_mysql 
			
			$tpl->DATA_CADASTRO = date('d/m/Y', $timestamp);
			
			$_SESSION['UserName'] = $linha['nome'];
			
			if($linha['foto_perfil'] === ""){
				$tpl->FOTO_PERFIL = 'avatar.png';
			}else{
				$tpl->FOTO_PERFIL = $linha['foto_perfil'];
			}
		}
	
	if($_SESSION['RoleUser'] == '1'){
		$tpl->block("BLOCO_CONFIGURACOES");	
	}
		/*	
	$token_corretora = $_SESSION['TokenCorretora'];
	$query_company = $pdo->query("SELECT background, logo, nome FROM cadastro_empresas WHERE token = '$token_corretora'")->fetchAll();
 		
		foreach($query_company as $ln){
			$nome = $ln['nome'];
			$background = $ln['background'];
			$logo = $ln['logo'];
		}
		
	$tpl->BACKGROUND = $background;
	if($logo != ""){
		$tpl->LOGO_EMPRESA = '<img src="'.ROOT.'/admin/imagens/'.$logo.'" class="img-responsive" alt="'.$nome.'" />';
	}else{
		$tpl->LOGO_EMPRESA = '<img src="'.ROOT.'/src/imagens/logo.png" class="img-responsive" alt="Simulador Group Saúde" />';	
	}
	*/
	$tpl->ROOT = ROOT;
		
    $tpl->DATA = date('Y');
	
	
	$tpl->NOME_USUARIO = $_SESSION['NameUser'];

    $tpl->show();

?>